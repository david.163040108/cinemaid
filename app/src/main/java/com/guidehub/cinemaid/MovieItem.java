package com.guidehub.cinemaid;

import java.io.Serializable;

public class MovieItem implements Serializable {

    private String id;
    private String judul;
    private String durasi;
    private String img_url;
    private String genre;
    private String sutradara;

    public MovieItem(String id, String judul, String durasi, String img_url, String genre, String sutradara) {
        this.id = id;
        this.judul = judul;
        this.durasi = durasi;
        this.img_url = img_url;
        this.genre = genre;
        this.sutradara = sutradara;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDurasi() {
        return durasi;
    }

    public void setDurasi(String durasi) {
        this.durasi = durasi;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getSutradara() {
        return sutradara;
    }

    public void setSutradara(String sutradara) {
        this.sutradara = sutradara;
    }
}
