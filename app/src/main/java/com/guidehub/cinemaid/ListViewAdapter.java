package com.guidehub.cinemaid;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

public class ListViewAdapter extends ArrayAdapter<MovieItem> {

    private List<MovieItem> playerItemList;

    private Context context;

    public ListViewAdapter(List<MovieItem> playerItemList, Context context) {
        super(context, R.layout.list_item, playerItemList);
        this.playerItemList = playerItemList;
        this.context = context;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);

        View listViewItem = inflater.inflate(R.layout.list_item, null, true);

//        TextView textViewId = listViewItem.findViewById(R.id.textViewId);
  //      TextView textViewJudul = listViewItem.findViewById(R.id.textViewJudul);
//        TextView textViewDurasi = listViewItem.findViewById(R.id.textViewDurasi);
//        TextView textViewGenre = listViewItem.findViewById(R.id.textViewGenre);
////        TextView textViewSutra = listViewItem.findViewById(R.id.textViewSutra);
   //     ImageView imgVIew = listViewItem.findViewById(R.id.Poster);


        MovieItem playerItem = playerItemList.get(position);

//        textViewId.setText(playerItem.getId());
  //      textViewJudul.setText(playerItem.getJudul());
//        textViewDurasi.setText(playerItem.getDurasi());
//        textViewGenre.setText(playerItem.getGenre());
//        textViewSutra.setText(playerItem.getSutradara());

     //   Glide.with(context).load(playerItem.getImg_url()).into(imgVIew);

        return listViewItem;
    }
}