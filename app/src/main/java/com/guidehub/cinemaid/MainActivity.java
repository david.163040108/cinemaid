package com.guidehub.cinemaid;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView profil,info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView bottomNav = findViewById(R.id.bottom_bar);
        bottomNav.setOnNavigationItemSelectedListener(navListener);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        profil = (ImageView) findViewById(R.id.btnProfile);
        info = (ImageView) findViewById(R.id.btnInfo);

        profil.setOnClickListener(this);
        info.setOnClickListener(this);


        //membuka awal film
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new FilmsFragment()).commit();
        }
    }



    private  BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment selectedFragment = null;

                    switch (menuItem.getItemId()){
                        case R.id.nav_film :
                            selectedFragment = new FilmsFragment();
                            break;
                        case R.id.nav_bioskop :
                            selectedFragment = new BioskopFragment();
                            break;
                        case R.id.nav_tiket :
                            selectedFragment = new TicketFragment();
                            break;
                    }

                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selectedFragment)
                            .commit();
                    return true;
                }
            };

    @Override
    public void onClick(View v) {
        Intent intent = null;
        if ( v == profil ){
            intent = new Intent(MainActivity.this, ProfileActivity.class);
            startActivity(intent);
        }else if ( v == info) {
            intent = new Intent(MainActivity.this, AboutActivity.class);
            startActivity(intent);
        }

    }
}
