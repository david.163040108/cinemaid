package com.guidehub.cinemaid;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.bumptech.glide.Glide;

import java.util.List;

public class Adapter extends PagerAdapter {
    private List<MovieItem> models;
    private LayoutInflater layoutInflater;
    private Context context;

//    public Adapter(List<MovieItem> models, Context context) {
//        this.models = models;
//        this.context = context;
//    }

    public Adapter(List<MovieItem> movieItems, Context context) {
        this.models =movieItems;
        this.context = context;
    }

    public Adapter(List<MovieItem> movieItems, Response.Listener<String> stringListener) {

    }

    @Override
    public int getCount() {
        return models.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.list_item, container, false);

        ImageView imageView;
        TextView title, desc;

        imageView = view.findViewById(R.id.image);
        title = view.findViewById(R.id.title);
        desc = view.findViewById(R.id.genre);

        //imageView.setImageResource(models.get(position).getImg_url());
        title.setText(models.get(position).getJudul());
        desc.setText(models.get(position).getGenre());
        Glide.with(context).load(models.get(position).getImg_url()).into(imageView);


        container.addView(view, 0);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(context, OrderBuyActivity.class);
//                intent.putExtra("param", models.get(position).getTitle());
//                context.startActivity(intent);
            }
        });
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }
}
