package com.guidehub.cinemaid;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FilmsFragment extends Fragment {

    ViewPager viewPager;
  //  Adapter adapter;
    List<MovieItem> movieItems;

    private ListView listViewItem ;
    private static final String URL_GET = "https://cinemaid21.000webhostapp.com/getFilm.php";
    private List<MovieItem> playerItemList;

    public FilmsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_films, container, false);

//
        viewPager = view.findViewById(R.id.viewPager);
        playerItemList = new ArrayList<>();
        movieItems = new ArrayList<>();
        loadPlayer();

        return  view;
    }
    private void loadPlayer() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading..");
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_GET,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("result");

                            progressDialog.dismiss();
                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject playerObject = jsonArray.getJSONObject(i);
                                String id = playerObject.getString("id");
                                String judul = playerObject.getString("judul");
                                String durasi = playerObject.getString("durasi");
                                String img_url = playerObject.getString("img_url");
                                String genre = playerObject.getString("genre");
                                String sutradara =playerObject.getString("sutradara");

                                MovieItem movieItem = new MovieItem(id, judul,durasi,img_url,genre,sutradara);

//                                playerItemList.add(movieItem);
                                playerItemList.add(movieItem);
                            }

                           Adapter adapter = new Adapter(playerItemList,getActivity());
                            viewPager.setAdapter(adapter);
                            viewPager.setPadding(0, 0, 270, 0);
//
//                            ListViewAdapter adapter = new ListViewAdapter(playerItemList, getActivity());
//
//                            listViewItem.setAdapter(adapter);

                        } catch (JSONException e) {
                            progressDialog.dismiss();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }

}
